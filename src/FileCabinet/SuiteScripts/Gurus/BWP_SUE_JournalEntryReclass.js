/*
 ***********************************************************************
 *
 * The following JavaScript code is created by GURUS Solutions,
 * a NetSuite Partner. It is a SuiteFlex component containing custom code
 * intended for NetSuite (www.netsuite.com) and use the SuiteScript API.
 * The code is provided "as is": GURUS Solutions shall not be liable
 * for any damages arising out the intended use or if the code is modified
 * after delivery.
 *
 * Company:     GURUS Solutions inc., www.gurussolutions.com
 *              Cloud Consulting Pioneers
 * Author:      geoffrey.wagner@gurussolutions.com
 * Date:        Jan 20, 2021 08:22:32 AM
 * File:        BWS_SUE_JournalEntryReclass.js
 ***********************************************************************/
/**
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */
define(['N/record', 'N/search', 'N/log', 'N/error'],
		function(recordModule, searchModule, logModule, errorModule) {

	const MEMO_PREFIX_PATTERNS = [];
	MEMO_PREFIX_PATTERNS.push(/^Created From Item Resale Cost Amount On Revenue Element #RE/g);
	MEMO_PREFIX_PATTERNS.push(/^Amortization Source/g);
	MEMO_PREFIX_PATTERNS.push(/^Amortization Destination/g);
	
	/** The following function is the entry point to run on beforeSubmit on Journal Entry transactions
	 *
	 * @author geoffrey.wagner@gurussolutions.com
	 * @param {nlobjContext} context : beforeSubmit trigger context
	 * @governance 20 units
	 */
	function beforeSubmit_JournalEntryReclass(context) {
		try {
			var jeRec = context.newRecord;
			logModule.debug({title:'context.type',details:context.type});
			
			// Generating the JEs through the Update Revenue Elements and Create Journal Entries native functionality doesn't hit the CREATE trigger
			var scriptFired = jeRec.getValue({fieldId: 'custbody_reclass_script_fired'});
			logModule.debug({title:'scriptFired',details:scriptFired});
			
			// Logic should only run once. CREATE trigger was not firing properly for script to trigger only in CREATE context. JE is "CREATED FROM REVENUE RECOGNITION SCHEDULE".
			if (!scriptFired) {
				
				// id/name of Code Interco Pour Retraitement on JE 
				var codeIntercoRetraitementObj = searchForCodeIntercoRetraitement(jeRec); // 10 units
				logModule.debug({title: 'codeIntercoRetraitementObj', details: JSON.stringify(codeIntercoRetraitementObj)});
				
				// id/name of Code Interco on all Subsidiaries
				var subsidiaryIntercoObj = searchForSubsidiaryCodeInterco(); // 10 units
				logModule.debug({title: 'subsidiaryIntercoObj', details: JSON.stringify(subsidiaryIntercoObj)});
				
				var codeInterco, codeIntercoRetraitement, memoPrefix, codeIntercoMapped;
				for (var i = 0; i < jeRec.getLineCount({sublistId: 'line'}); i++) {
					codeInterco = parseInt(jeRec.getSublistValue({sublistId: 'line', fieldId: 'cseg_bwp_codeinterc', line: i}));
					codeIntercoRetraitement = jeRec.getSublistValue({sublistId: 'line', fieldId: 'cseg_bwp_interco_rm', line: i});
					memoPrefix = memoPrefixAccepted(jeRec.getSublistValue({sublistId: 'line', fieldId: 'memo', line: i}));
					// Match text of Code Interco Pour Retraitement & Code Interco to get the internalid to set
					codeIntercoMapped = getIntercoId({codeIntercoRetraitement: codeIntercoRetraitement, codeIntercoRetraitementObj: codeIntercoRetraitementObj, codeIntercoObj: subsidiaryIntercoObj});
					logModule.debug({title:'codeInterco, codeIntercoRetraitement, memoPrefix, codeIntercoMapped', details: codeInterco+', '+codeIntercoRetraitement+', '+memoPrefix+', '+codeIntercoMapped});
					// If the CodeIntecoCCA exists on a Subsidiary, is different than the CodeInterco, and has an accepted Memo prefix, and is on a Subsidiary
					if (subsidiaryIntercoObj[codeInterco] != codeIntercoRetraitementObj[codeIntercoRetraitement] && memoPrefix && codeIntercoMapped != null) {
						jeRec.setSublistValue({sublistId: 'line', fieldId: 'cseg_bwp_codeinterc', line: i, value: codeIntercoMapped});
//						jeRec.setSublistValue({sublistId: 'line', fieldId: 'eliminate', line: i, value: true}); // To be re-added at a later date.
					}
				}

//				jeRec.setValue({fieldId: 'tosubsidiary', value: 3}); // TEMPORARY VISIATIV SOFTWARE - What value should be here?
				// Set checkbox at end of script execution to not retrigger if there are other modifications to the JE
				jeRec.setValue({fieldId: 'custbody_reclass_script_fired', value: true});
			}
		} catch(e) {
			logModule.error({title: e.name, details: e.stack}); // There was an UNEXPECTED_ERROR that I could not reproduce. STACK used instead of MESSAGE for debugging purposes
		}
	}

	/** The following function returns the internal id of the Code Interco that matches
	 *
	 * @author geoffrey.wagner@gurussolutions.com
	 * @param {Object} options								: Contains objects necessary for determining the internalid of the Code Interco to set
	 * @param {String} options.codeIntercoRetraitement		: Code Interco Pour Retraitement id
	 * @param {Object} options.codeIntercoObj				: Id/Name of Code Interco available on a Subsidiary
	 * @param {Object} options.codeIntercoRetraitementObj	: Id/Name of Code Interco Pour Retraitement
	 * @return {String} codeIntercoId 						: Code Interco id
	 * @governance 0 units
	 */
	function getIntercoId(options) {
		var codeIntercoRetraitement = options.codeIntercoRetraitement;
		var codeIntercoObj = options.codeIntercoObj;
		var codeIntercoRetraitementObj = options.codeIntercoRetraitementObj;
		
		var codeIntercoId = null;
		// Using the Code Interco Pour Retraitement segment id, determine the id of the Code Interco segment id through text matching
		for (var code in codeIntercoObj) {
			if (codeIntercoObj[code] == codeIntercoRetraitementObj[codeIntercoRetraitement]) {
				codeIntercoId = code;
				break;
			}
		}
		
		return codeIntercoId;
	}
	
	/** The following function returns whether the memo has an accepted prefix for solution logic
	 *
	 * @author geoffrey.wagner@gurussolutions.com
	 * @param {String} 		memo			: Memo from JE line
	 * @return {Boolean} 	acceptedPrefix	: Prefix accepted for logic on that JE line
	 * @governance 0 units
	 */
	function memoPrefixAccepted(memo) {		
		var acceptedPrefix = false;
		var regEx;
		
		for (var i = 0; i < MEMO_PREFIX_PATTERNS.length; i++) {
			regEx = new RegExp(MEMO_PREFIX_PATTERNS[i]);
			if (regEx.test(memo)) {
				acceptedPrefix = true;
				
				return acceptedPrefix;
			}
		}
		
		return acceptedPrefix;
	}
	
	/** The following function searches for all Code Interco Pour Retraitement (cseg_bwp_interco_rm) values for text matching with JE's Code Interco
	 *  Code Interco Pour Retraitement CCA id is not the same as the Code Interco segments
	 *
	 * @author geoffrey.wagner@gurussolutions.com
	 * @param {nlobjRecord} jeRec		: NS Journal Entry newRecord
	 * @return {Object} codeIntercoObj	: JSON object of interco id (key) & name (value)
	 * @governance 10 units
	 */
	function searchForCodeIntercoRetraitement(jeRec) {
		var codeIntercoObj = {};
		var intercoRetraitementArray = [];
		var intercoId;

		// Create array of unique "Code Interco Pour Retraitement" custom segment
		for (var i = 0; i < jeRec.getLineCount({sublistId: 'line'}); i++) {
			intercoId = jeRec.getSublistValue({sublistId: 'line', fieldId: 'cseg_bwp_interco_rm', line: i});
			if (intercoRetraitementArray.indexOf(intercoId) == -1) {
				intercoRetraitementArray.push(intercoId);
			}
		}
		
		if (intercoRetraitementArray.length <= 0) {
			return codeIntercoObj;
		}
		
		var filters = [];
		filters.push(searchModule.createFilter({name: 'internalid', operator: searchModule.Operator.ANYOF, values: intercoRetraitementArray}));
		
		var columns = [];
		columns.push(searchModule.createColumn({name: 'internalid'}));
		columns.push(searchModule.createColumn({name: 'name'}));
		
		// Search on Code Interco pour Retraitement
		var codeIntercoSearch = searchModule.create({type: 'customrecord_cseg_bwp_interco_rm', filters: filters, columns: columns}); // 10 units
		
		// Run search and generate object with values to match
		codeIntercoSearch.run().each(function(code) {
			if (!codeIntercoObj.hasOwnProperty(code.getValue({name: 'internalid'}))) {
				codeIntercoObj[code.getValue({name: 'internalid'})] = code.getValue({name: 'name'});
			}
			
	        return true;
	    });
		
		return codeIntercoObj;
	}
	
	/** The following function searches for all Subsidiary Code Interco (cseg_bwp_codeinterc) values for text matching with JE's Code Interco Pour Retraitment CCA
	 *  Subsidiary has Code Interco, which has a different id but the same text value as some Code Interco Pour Retraitement CCA
	 *
	 * @author geoffrey.wagner@gurussolutions.com
	 * @return {Object} subsidiaryObj	: JSON object of interco id (key) & name (value)
	 * @governance 10 units
	 */
	function searchForSubsidiaryCodeInterco() {
		// Get all Subsidiaries to compare text values as validation. Unable to add multiple text comparison filters to search directly.
		var filters = [];
		
		var columns = [];
		columns.push(searchModule.createColumn({name: 'internalid'}));
		columns.push(searchModule.createColumn({name: 'name'}));
		
		var codeIntercoSearch = searchModule.create({type: 'customrecord_cseg_bwp_codeinterc', filters: filters, columns: columns}); // 10 units
		
		// Run search and generate object with all 
		var intercoValues = {};
		var intercoId;
		codeIntercoSearch.run().each(function(codeInterco) {
			intercoId = codeInterco.getValue({name: 'internalid'});
			if (intercoId) {
				intercoValues[intercoId] = codeInterco.getValue({name: 'name'});
			}
	        
	        return true;
	    });
		
		return intercoValues;	
	}

	function logRecord(record, logTitle) {
		var logRecord = JSON.stringify(record) || 'UNDEFINED';
		var recNum = 0;
		while (logRecord.length > 0) {
			recNum = recNum + 1;
			log.debug({
				title: logTitle + ' record - ' + recNum,
				details: logRecord
			});
			logRecord = logRecord.substring(3900);
		}
	}
	
	function logVar(value, title) {
		log.debug({
			title: title + ' (' + typeof value + ')',
			details: value
		});
	}

	return {
		beforeSubmit: beforeSubmit_JournalEntryReclass
	};
});