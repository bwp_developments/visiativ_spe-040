/*
 ***********************************************************************
 *
 * The following JavaScript code is created by GURUS Solutions,
 * a NetSuite Partner. It is a SuiteFlex component containing custom code
 * intended for NetSuite (www.netsuite.com) and use the SuiteScript API.
 * The code is provided "as is": GURUS Solutions shall not be liable
 * for any damages arising out the intended use or if the code is modified
 * after delivery.
 *
 * Company:     GURUS Solutions inc., www.gurussolutions.com
 *              Cloud Consulting Pioneers
 * Author:      geoffrey.wagner@gurussolutions.com
 * Date:        Jan 20, 2021 08:40:10 AM
 * File:        BWS_SUE_RevenueArrangementResaleCalc.js
 ***********************************************************************/
/**
 *@NApiVersion 2.1
 *@NScriptType UserEventScript
 */
define(['N/record', 'N/log', 'N/error', 'N/query', 'N/search', 'N/format'],
		function(recordModule, log, errorModule, query, searchModule, format) {

	/** The following function is the entry point to run on beforeSubmit on Revenue Arrangements on Create
	 *  The script will calculate and set the Item Resale amount on the Revenue Arrangement, as well as update
	 *  Sales Orders' Code Interco pour Retraitement where appropriate.
	 *
	 * @author geoffrey.wagner@gurussolutions.com
	 * @param {nlobjContext} context : beforeSubmit trigger context
	 * @governance 21 units
	 */
	function beforeSubmit_RevenueArrangementResaleCalc(context) {
		log.debug('start before submit');
		try {
			if ([context.UserEventType.CREATE, context.UserEventType.EDIT].includes(context.type)) {
				var revArrRec = context.newRecord;
				// logRecord(revArrRec, 'revArrRec');

				// Search for SO and Item details
				var updateObj = searchForUpdateDetails({revArrRec: revArrRec}); // 21 units
				log.debug({ title:'updateObj', details:`${revArrRec.getValue({ fieldId: 'tranid' })} (${revArrRec.getValue({ fieldId: 'id' })}) - ${JSON.stringify(updateObj)}` });

				// Update fields on Revenue Arrangement, which updates the associated Revenue Elements
				updateRevenueArrangement({updateObj: updateObj, revArrRec: revArrRec});              
			}

		} catch (e) {
			log.error({title: e.code, details: e.stack});
		}		
		log.debug('end before submit');
	}

	/** The following function updates the Revenue Arrangement with the information gathered from
	 *  the Sales Orders and Items
	 *
	 * @author geoffrey.wagner@gurussolutions.com
	 * @param {Object} options				: object containing data to update and record to update
	 * @param {Object} options.updateObj	: object containing data to update, key = lineuniquekey (SO) / sourceid (RA)
	 * @param {Object} options.revArrRec	: Revenue Arrangement transaction record
	 * @governance 0 units
	 */
	function updateRevenueArrangement(options) {
		var updateObj = options.updateObj;
		var revArrRec = options.revArrRec;
		// Eric Moulin - 22/03/2022 - Begin
		if (updateObj.contractCostAccrualDate){
			revArrRec.setValue({
				fieldId: 'contractcostaccrualdate',
				value: updateObj.contractCostAccrualDate
			});
		}
		// Eric Moulin - 22/03/2022 - End

		// Loop through RA lines to update values
		var key, resaleCostAmount;
		var lineCount = revArrRec.getLineCount({sublistId: 'revenueelement'});
		for (var i = 0; i < lineCount; i++) {

			// If there is data to update for the line from the SO search
			key = revArrRec.getSublistValue({sublistId: 'revenueelement', line: i, fieldId: 'sourceid'});
			log.debug({title: 'key', details: key});
			if (key in updateObj) {
				resaleCostAmount = calculateItemResaleCostAmount(updateObj[key]);
				revArrRec.setSublistValue({sublistId: 'revenueelement', line: i, fieldId: 'itemresalecostamount', value: resaleCostAmount});
			} else {
				log.audit({title: 'No data to update', details: 'No data found from related SO to update. Revenue Arrangement sourceid: ' + key});
			}

		}
	}

	/** The following function collects SO ids to search on, searches on SOs to get information from
	 *  the transaction, vendor and included items. It then returns a formatted object with the information required
	 *  to update the Revenue Arrangement and its related Revenue Elements.
	 *
	 * @author geoffrey.wagner@gurussolutions.com modified by grant.encarnacion@gurussolutions.com
	 * @param {Object} options				: object interco code / subsidiary data to include, and record to update
	 * @param {Object} options.revArrRec	: Revenue Arrangement transaction record
	 * @return {Object} soObj				: JSON object of SO, Item, Vendor information
	 * @governance 21 units
	 */
	function searchForUpdateDetails(options) {
		var revArrRec = options.revArrRec;

		// Gather unique SO ids to search
		var soIds = [];
		var soIdRaw, soId;
		var pattern = /\d+$/g; // Pattern : 1+ digits at end of string. Ex: SalesOrd_4992 = 4992.

		// sourceheaderid (SO id) returns undefined in standard/dynamic mode, and is an invalid column during searches
		var revElemLineCount = revArrRec.getLineCount({sublistId: 'revenueelement'});
		for (var i = 0; i < revElemLineCount; i++) {
			// Only SOs will be on the revenueelement sublist
			soIdRaw = revArrRec.getSublistValue({sublistId: 'revenueelement', fieldId: 'referenceid', line: i});
			soId = soIdRaw.match(pattern)[0]; // Need to parse a string to retrieve id

			// Create array of unique SO ids
			if (soIds.indexOf(soId) == -1) {
				soIds.push(soId);
			}
		}

		// Create search for Sales Order & Item info
		var soObj = {};
		if (soIds.length > 0) {

			// Subsidiary currency is needs to be taken from the RA record's subsidiary, and not the Sales Order.
			var RASubsidiary = revArrRec.getValue({
				fieldId: 'subsidiary'
			});
			var subsidiaryLookupObj = searchModule.lookupFields({
			    type: searchModule.Type.SUBSIDIARY,
			    id: RASubsidiary,
			    columns: ['currency']
			}); // 1 unit
			var subsidiaryCurrency = subsidiaryLookupObj['currency'][0].value;

			var filters = [];
			filters.push(searchModule.createFilter({ name: 'mainline', operator: searchModule.Operator.IS, values: false }));
			filters.push(searchModule.createFilter({ name: 'internalid', operator: searchModule.Operator.ANYOF, values: soIds }));
			filters.push(searchModule.createFilter({ name: 'taxline', operator: searchModule.Operator.IS, values: false }));

			var columns = [];
			columns.push(searchModule.createColumn({name: 'lineuniquekey'})); // SO's lineuniquekey = Revenue Arrangement sourceid
			 // 'povendor' non-searchable, can't just join to get the Vendor Code Interco
			columns.push(searchModule.createColumn({name: 'purchaseOrder'}));
//			columns.push(searchModule.createColumn({name: 'intercoexpenseaccount', join: 'item'})); // Could not update this value on Revenue Arrangement
	        columns.push(searchModule.createColumn({name: 'currency', join: 'subsidiary'}));
			columns.push(searchModule.createColumn({name: 'exchangerate', join: 'purchaseOrder'}));
			columns.push(searchModule.createColumn({name: 'currency', join: 'purchaseOrder'}));
			columns.push(searchModule.createColumn({name: 'exchangerate'}));
			columns.push(searchModule.createColumn({name: 'porate'}));
			columns.push(searchModule.createColumn({name: 'quantity'}));
			columns.push(searchModule.createColumn({name: 'currency'}));
			columns.push(searchModule.createColumn({name: 'internalid'}));
			columns.push(searchModule.createColumn({name: 'cseg_bwp_interco_rm'}));
			// Eric Moulin - 22/03/2022 - Begin
			columns.push(searchModule.createColumn({name: 'custcol_bwp_so_po_link'}));
			// Eric Moulin - 22/03/2022 - End

			var soSearch = searchModule.create({type: searchModule.Type.SALES_ORDER, filters: filters, columns: columns}); // 10 units

			// Information from the Vendor is not accessible through joins on the SO. Get PO ids to do a search for Vendor info
			var poIds = [];

			// Run search and generate object with values to update
			soSearch.run().each(function(so) {				
				let poId;
				// Only lines where there is a PO Rate are considered for the updates
				if (so.getValue({name: 'porate'}) != null && so.getValue({name: 'porate'}) != '') {
					poId = so.getValue({name: 'purchaseOrder'});

					// Unique Purchase Order ids
					if (poIds.indexOf(poId) == -1) {
						poIds.push(poId);
					}
				}

				soObj[so.getValue({name: 'lineuniquekey'})] = {
					poId: poId,
					vendorCurrency: '', // Devise étrangère (Vendor Currency)
//			        	intercoExpenseAccount: so.getValue({name: 'intercoexpenseaccount', join: 'item'}), // Could not update this value on Revenue Arrangement / Revenue Element
					subsidiaryCurrency: parseInt(subsidiaryCurrency)/**parseInt(lookupFIeld on Subsidiary for Currency)**/, // Devise de comptabilisation (Subsidiary Currency)
					poExchangeRate: parseFloat(so.getValue({ name: 'exchangerate', join: 'purchaseOrder' })), // taux de change en entête de commande d'achat (PO exchange rate)
					poCurrency: parseInt(so.getValue({ name: 'currency', join: 'purchaseOrder' })), // Devise d'achat (PO Currency)
					soExchangeRate: parseFloat(so.getValue({ name: 'exchangerate' })), // taux de change en entête de commande d'achat (SO exchange rate)
					poRate: parseFloat(so.getValue({ name: 'porate' })),
					quantity: parseInt(so.getValue({ name: 'quantity' })),
					soCurrency: parseInt(so.getValue({ name: 'currency' })), // Devise de vente (SO Currency)
					soId: parseInt(so.getValue({ name: 'internalid' })),
					lineCodeIntercoRetraitement: so.getValue({ name: 'cseg_bwp_interco_rm' }),
					// Code Interco and Code Interco pour Retraitement are 2 different segments. This id will be populated with the id with matching text
					//codeIntercoConverted: '' // Code Interco Pour Retraitement internalid of the matching (text) Code Interco from the Vendor

					// Eric Moulin - 22/03/2022 - Begin
					sopolink: so.getValue({ name: 'custcol_bwp_so_po_link' })
					// Eric Moulin - 22/03/2022 - End
				};

		        return true;
		    });
		}
		
		// Eric Moulin - 22/03/2022 - Begin

		// // Get Vendor details using PO ids
		// var vendorObj = getVendorDetails(poIds); // 10 units

		// Insert the Vendor details to the Sales Order object
		// for (var soLine in soObj) {
		// 	poId = soObj[soLine].poId;
		// 	if (vendorObj.hasOwnProperty(poId)) {
		// 		soObj[soLine].vendorCurrency = vendorObj[poId].currency;
		// 	}
		// }

		let poInfos = getPoInfos(poIds);
		let receiptDates = poInfos.filter(x => x.receiptDate > 0).map(x => x.receiptDate);
		if (receiptDates.length > 0) {
			soObj.contractCostAccrualDate = new Date(Math.max(...receiptDates));
		}

		for (let soLine in soObj) {
			let sopolink = soObj[soLine].sopolink;
			let poInfo = poInfos.find(element => element.sopolink == sopolink);
			if (poInfo) {
				soObj[soLine].vendorCurrency = poInfo.currency;
				soObj[soLine].receiptQty = poInfo.receiptQty;
				soObj[soLine].receiptDate = poInfo.receiptDate;
			}
		}
		// Eric Moulin - 22/03/2022 - End

		return soObj;
	}

	function getPoInfos(poIds) {
		let poInfos = [];
		if (!poIds || poIds.length == 0) { return poInfos; }

		let sqlString = `	Select t1.id, t1.entity, t4.entitytitle, t4.currency, t2.custcol_bwp_so_po_link, t2.quantityshiprecv, t3.receiptdate
							From transaction t1
								Inner Join transactionline t2
								  On t2.transaction = t1.id
								Left Outer Join (
									Select t2.createdfrom, max(t1.trandate) as receiptdate
									From transaction t1
										Inner Join transactionline t2
										  On t2.transaction = t1.id
									Where t1.recordtype = 'itemreceipt'
									Group By t2.createdfrom ) t3 
								  On t3.createdfrom = t1.id
								Left Outer Join vendor t4 
								  On t4.id = t1.entity
							Where t2.custcol_bwp_so_po_link is not null
							  And t1.id in (${poIds.join(',')}) `;

		let resultSet = query.runSuiteQL({ query: sqlString }).asMappedResults();
		resultSet.forEach(result => {
			let receiptDate = 0;
			if (result['receiptdate']) {
				receiptDate = format.parse({ value: result['receiptdate'], type: format.Type.DATE });
			}
			poInfos.push({
				sopolink: result['custcol_bwp_so_po_link'],
				currency: result['currency'],
				receiptDate: receiptDate,
				receiptQty: result['quantityshiprecv']
			});
		});
		// log.debug({
		// 	title: 'poInfos',
		// 	details: poInfos
		// });
		return poInfos;
	}

	/** The following function searches for Vendor details
	 *
	 * @author geoffrey.wagner@gurussolutions.com
	 * @param {Array} 	poIds		: Purchase Order internalids
	 * @return {Object} vendorObj	: Object containing Vendor details with key of PO id
	 * @governance 10 units
	 */
	function getVendorDetails(poIds) {

		var filters = [];
		filters.push(searchModule.createFilter({name: 'mainline', operator: searchModule.Operator.IS, values: true}));
		filters.push(searchModule.createFilter({name: 'internalid', operator: searchModule.Operator.ANYOF, values: poIds}));

		var columns = [];
		columns.push(searchModule.createColumn({name: 'internalid'}));
		columns.push(searchModule.createColumn({name: 'currency', join: 'vendor'}));

		var vendorSearch = searchModule.create({type: searchModule.Type.PURCHASE_ORDER, filters: filters, columns: columns}); // 10 units

		var vendorObj = {};
		vendorSearch.run().each(function(vendor) {
			vendorObj[vendor.getValue({name: 'internalid'})] = {
				currency: vendor.getValue({name: 'currency', join: 'vendor'})
			};

			return true;
		});

		return vendorObj;
	}

	/** The following function returns the Resale Cost Amount to be set on the Revenue Arrangement line
	 * "Devise de Comptabilisation" refers to if the comparing currency matches the subsidiary currency. If they do not match, it is considered to be a "Devise �trang�re"
	 *
	 * @author geoffrey.wagner@gurussolutions.com modified by grant.encarnacion@gurussolutions.com
	 * @param {Object} updateLineObj	: object containing information from the RA line's corresponding SO/Vendor/Subsidiary/Item
	 * @return {Float} resaleCostAmount	: resale cost amount
	 * @governance 0 units
	 */
	function calculateItemResaleCostAmount(updateLineObj) {
		// Eric Moulin - 22/03/2022 - Begin
		// var resaleCostAmount = 0;
		var resaleCostAmount = 0;
		if (updateLineObj.poRate) {
			if (!updateLineObj.receiptQty) {
				var message =  `SO ${updateLineObj.soId}: No quantity received for PO ${updateLineObj.poId}`;
				if (!updateLineObj.sopolink) {
					message += ' - custcol_bwp_so_po_link is not populated';
				}
				log.error({
					title: 'Invalid PO infos',
					details: message
				});
			}
			resaleCostAmount = updateLineObj.receiptQty * updateLineObj.poRate;
		}
		// resaleCostAmount = updateLineObj.quantity * updateLineObj.poRate;
		// Eric Moulin - 22/03/2022 - End

		if (resaleCostAmount){
			// SO : Devise de Comptabilisation
			// PO : Devise de Comptabilisation
			if (updateLineObj.soCurrency == updateLineObj.subsidiaryCurrency && updateLineObj.poCurrency == updateLineObj.subsidiaryCurrency) {
				// Eric Moulin - 22/03/2022 - Begin
				// resaleCostAmount = updateLineObj.quantity * updateLineObj.poRate;
				// Eric Moulin - 22/03/2022 - End
			}
			// SO : Devise étrangère
			// PO : Devise étrangère
			else if (updateLineObj.soCurrency != updateLineObj.subsidiaryCurrency && updateLineObj.poCurrency != updateLineObj.subsidiaryCurrency){
				// Eric Moulin - 22/03/2022 - Begin
				// resaleCostAmount = updateLineObj.quantity * updateLineObj.poRate * updateLineObj.soExchangeRate;
				// Eric Moulin - 07/03/2023 - Begin
				// Do not apply exchange rate, amount retrieved by the search already matches the target currency
				// resaleCostAmount *= updateLineObj.soExchangeRate
				// Eric Moulin - 07/03/2023 - Begin
				// Eric Moulin - 22/03/2022 - End
			}
			// SO : Devise de Comptabilisation
			// PO : Devise étrangère
			else if (updateLineObj.soCurrency == updateLineObj.subsidiaryCurrency && updateLineObj.poCurrency != updateLineObj.subsidiaryCurrency){
				// Eric Moulin - 22/03/2022 - Begin
				// resaleCostAmount = updateLineObj.quantity * updateLineObj.poRate * updateLineObj.poExchangeRate;
				resaleCostAmount *= updateLineObj.soExchangeRate
				// Eric Moulin - 22/03/2022 - End
			}
			// SO : Devise étrangère
			// PO : Devise de Comptabilisation
			else if (updateLineObj.soCurrency != updateLineObj.subsidiaryCurrency && updateLineObj.poCurrency == updateLineObj.subsidiaryCurrency){
				// Eric Moulin - 22/03/2022 - Begin
				// resaleCostAmount = updateLineObj.quantity * updateLineObj.poRate;
				// Eric Moulin - 22/03/2022 - End
			}
		}

		return resaleCostAmount.toFixed(2);
	}

	function logRecord(record, logTitle) {
		var logRecord = JSON.stringify(record) || 'UNDEFINED';
		var recNum = 0;
		while (logRecord.length > 0) {
			recNum = recNum + 1;
			log.debug({
				title: logTitle + ' record - ' + recNum,
				details: logRecord
			});
			logRecord = logRecord.substring(3900);		
		}
	}

	return {
		beforeSubmit: beforeSubmit_RevenueArrangementResaleCalc
	};
});