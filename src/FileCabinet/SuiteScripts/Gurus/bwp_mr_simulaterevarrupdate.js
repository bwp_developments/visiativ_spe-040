var CACHEMODULE, ERRORMODULE, FILEMODULE, FORMATMODULE, RECORDMODULE, RUNTIMEMODULE, SEARCHMODULE;

/**
 * This script is in charge to generate a temporary Customer_nnn file in Work forlder
 * @NApiVersion 2.1
 * @NScriptType MapReduceScript
 */
define(['N/cache', 'N/error', 'N/file', 'N/format', 'N/record', 'N/runtime', 'N/search'], runMapReduce);

function runMapReduce(cache, error, file, format, record, runtime, search) {
	CACHEMODULE= cache;
	ERRORMODULE= error;
	FILEMODULE= file;
	FORMATMODULE= format;
	RECORDMODULE= record;
	RUNTIMEMODULE= runtime;
	SEARCHMODULE= search;
	
	let returnObj = {};
	returnObj['getInputData'] = _getInputData;
	returnObj['map'] = _map;
	// returnObj['reduce'] = _reduce;
	returnObj['summarize'] = _summarize;
	return returnObj;
}

/**
 * Defines the function that is executed at the beginning of the map/reduce process and generates the input data.
 * @param {Object} inputContext
 * @param {boolean} inputContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Object} inputContext.ObjectRef - Object that references the input data
 * @typedef {Object} ObjectRef
 * @property {string|number} ObjectRef.id - Internal ID of the record instance that contains the input data
 * @property {string} ObjectRef.type - Type of the record instance that contains the input data
 * @returns {Array|Object|Search|ObjectRef|File|Query} The input data to use in the map/reduce process
 * @since 2015.2
 */
function _getInputData(inputContext) {	
	log.debug('getInputData started');

	let searchObj = SEARCHMODULE.create({
		type: SEARCHMODULE.Type.REVENUE_ARRANGEMENT,
		filters: [
			['revenueplanstatus', 'noneof', 'COMPLETED'],
			'AND',
			['mainline', 'is', 'T'],
			// 'AND',
			// ['internalid', 'is', '47544']
		],
		columns: ['internalid', 'tranid']
	});	
	
	log.debug('getInputData done');
	return searchObj;
}

/**
 * Defines the function that is executed when the map entry point is triggered. This entry point is triggered automatically
 * when the associated getInputData stage is complete. This function is applied to each key-value pair in the provided
 * context.
 * @param {Object} mapContext - Data collection containing the key-value pairs to process in the map stage. This parameter
 *     is provided automatically based on the results of the getInputData stage.
 * @param {Iterator} mapContext.errors - Serialized errors that were thrown during previous attempts to execute the map
 *     function on the current key-value pair
 * @param {number} mapContext.executionNo - Number of times the map function has been executed on the current key-value
 *     pair
 * @param {boolean} mapContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} mapContext.key - Key to be processed during the map stage
 * @param {string} mapContext.value - Value to be processed during the map stage
 * @since 2015.2
 */
function _map(context) {
	// log.debug('map started');
	// logRecord(context, 'map context');
	let mapValue = JSON.parse(context.value);
	log.debug({
		title: 'map',
		details: `Simulate update on revenue arrangement ${mapValue.values.tranid} (${mapValue.id})`
	});
	let recordObj = RECORDMODULE.load({
		type: RECORDMODULE.Type.REVENUE_ARRANGEMENT,
		id: mapValue.id,
		isDynamic: false
	});
	try {
		recordObj.save();
	} catch (ex) {
		throw `${mapValue.values.tranid} (${mapValue.id}) : ${ex.name} - ${ex.message}`;
	}
	
	//log.debug('map done');
}

/**
 * Defines the function that is executed when the reduce entry point is triggered. This entry point is triggered
 * automatically when the associated map stage is complete. This function is applied to each group in the provided context.
 * @param {Object} reduceContext - Data collection containing the groups to process in the reduce stage. This parameter is
 *     provided automatically based on the results of the map stage.
 * @param {Iterator} reduceContext.errors - Serialized errors that were thrown during previous attempts to execute the
 *     reduce function on the current group
 * @param {number} reduceContext.execut ionNo - Number of times the reduce function has been executed on the current group
 * @param {boolean} reduceContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {string} reduceContext.key - Key to be processed during the reduce stage
 * @param {List<String>} reduceContext.values - All values associated with a unique key that was passed to the reduce stage
 *     for processing
 * @since 2015.2
 */
function _reduce(context) {
	// log.debug('reduce started');
	
	//log.debug('reduce done');
}

/**
 * Defines the function that is executed when the summarize entry point is triggered. This entry point is triggered
 * automatically when the associated reduce stage is complete. This function is applied to the entire result set.
 * @param {Object} summaryContext - Statistics about the execution of a map/reduce script
 * @param {number} summaryContext.concurrency - Maximum concurrency number when executing parallel tasks for the map/reduce
 *     script
 * @param {Date} summaryContext.dateCreated - The date and time when the map/reduce script began running
 * @param {boolean} summaryContext.isRestarted - Indicates whether the current invocation of this function is the first
 *     invocation (if true, the current invocation is not the first invocation and this function has been restarted)
 * @param {Iterator} summaryContext.output - Serialized keys and values that were saved as output during the reduce stage
 * @param {number} summaryContext.seconds - Total seconds elapsed when running the map/reduce script
 * @param {number} summaryContext.usage - Total number of governance usage units consumed when running the map/reduce
 *     script
 * @param {number} summaryContext.yields - Total number of yields when running the map/reduce script
 * @param {Object} summaryContext.inputSummary - Statistics about the input stage
 * @param {Object} summaryContext.mapSummary - Statistics about the map stage
 * @param {Object} summaryContext.reduceSummary - Statistics about the reduce stage
 * @since 2015.2
 */
function _summarize(summary) {
	log.debug('summary started');
//	log.debug('summary', summary);

	let processSummaryText = '';
	let errorMessage = '';
	if (processSummaryErrors(summary) > 0) {
		errorMessage = 'Errors during data import - see error file for details';
	} else {
	}
	
	// summary.output.iterator().each((key, value) => {
	// 	// logRecord('key', key);
	// 	// logRecord('value', value);
	// 	processSummaryText = processSummaryText || `Transactions created: \n`;
	// 	processSummaryText += `- ${value}\n`;
	// 	return true;
	// });
	
	log.debug('summary done');
}

function processMapValues(mapValues, scriptParams) {
	let dataMap = mapData(mapValues, scriptParams);
	applyFunctions(dataMap);
	return dataMap;
}

function processSummaryErrors(summary, extractionId, error) {
	let errorCount = 0;
	summary.mapSummary.errors.iterator().each(
		function(key, error, executionNo) {
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
            log.error({
                title: 'mapError',
                details: message
            });
			errorCount++;
			return true;
		}
	);
	summary.reduceSummary.errors.iterator().each(
		function(key, error, executionNo) {
			const errorObj = JSON.parse(error);
			let message;
			if (errorObj.message && errorObj.name && errorObj.name.startsWith('BWP_')) {
				message = errorObj.message;
			} else {
				message = error;
			}
            log.error({
                title: 'reduceError',
                details: message
            });
			errorCount++;
			return true;
		}
	);
	return errorCount;
}

function logRecord(record, logTitle) {
	let logRecord = JSON.stringify(record) || 'UNDEFINED';
	let recNum = 0;
	while (logRecord.length > 0) {
		recNum = recNum + 1;
		log.debug({
			title: logTitle + ' record - ' + recNum,
			details: logRecord
		});
		logRecord = logRecord.substring(3900);
	}
}

function logVar(value, title) {
	log.debug({
		title: title + ' (' + typeof value + ')',
		details: value 
	});	
}