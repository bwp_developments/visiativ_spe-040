/*
 ***********************************************************************
 *
 * The following JavaScript code is created by GURUS Solutions,
 * a NetSuite Partner. It is a SuiteFlex component containing custom code
 * intended for NetSuite (www.netsuite.com) and use the SuiteScript API.
 * The code is provided "as is": GURUS Solutions shall not be liable
 * for any damages arising out the intended use or if the code is modified
 * after delivery.
 *
 * Company:     GURUS Solutions inc., www.gurussolutions.com
 *              Cloud Consulting Pioneers
 * Author:      grant.encarnacion@gurussolutions.com
 * Date:        Mar 5, 2021 9:59:47 AM
 * File:        BWP_SUE_SOCodeIntercoCheck.js
 ***********************************************************************/
/**
 *@NApiVersion 2.0
 *@NScriptType UserEventScript
 */
define(['N/log', 'N/record', 'N/search'], function(logModule, recordModule, searchModule) {

	/** The following function triggers on the beforeSubmit entry point on Sales Orders on Create and Edit.
	 * 	For all line items that do not have the Code Interco pour Retraitement line field populated,
	 *  populate the field with the PO Vendor's Code Interco
	 *
	 * @author grant.encarnacion@gurussolutions.com
	 * @param context {context} : the default context object
	 * @governance 20 units
	 */
	function beforeSubmit_CodeIntercoCheck(context){

		if(context.type == context.UserEventType.CREATE || context.type == context.UserEventType.EDIT){

			var soRec = context.newRecord;

			// Loop over items sublist and create an array of UNIQUE Vendor Ids to search for.
			var uniqueVendorsList = [];
			var itemLineCount = soRec.getLineCount({sublistId: 'item'});
			log.debug('linecount item sublist', itemLineCount);

			for (var i = 0; i < itemLineCount; i++){
				var currentLineIntercoRM = soRec.getSublistValue({sublistId: 'item', fieldId: 'cseg_bwp_interco_rm', line: i});
				var currentLinePOVendorId = soRec.getSublistValue({sublistId: 'item', fieldId: 'povendor', line: i});

				// only consider lines with empty Code Interco Retraitement
				if (!currentLineIntercoRM && uniqueVendorsList.indexOf(currentLinePOVendorId) == -1){
					uniqueVendorsList.push(currentLinePOVendorId);
				}
			}

			if (uniqueVendorsList <= 0){
				log.debug('Terminating Script.', 'No lines to update.');
				return;
			}

			var vendorDetailsObj = getVendorDetails(uniqueVendorsList); // 10 units

			log.debug('vendorDetailsObj', vendorDetailsObj);

			// Map vendor Code Interco to SO Code Interco pour Retraitement Ids

			var convertedVendorDetailsObj = updateCodeIntercoConverted(vendorDetailsObj); // 10 units

			log.debug('convertedVendorDetailsObj' , convertedVendorDetailsObj);

			updateLineItemsCodeIntercoRM(soRec, convertedVendorDetailsObj);

		}
	}
	/** The following function searches for Vendor details to retrieve the codeInterco text and internal values for requested VendorIds
	 *
	 * @author grant.encarnacion@gurussolutions.com
	 * @param vendorIds {Array}: Array containing vendor internal IDs to search
	 * @return vendorDetails {object} : Key-value map containing details per Vendor ID
	 * 										Key: Vendor Internal ID
	 * 										Value: {Object} Code Interco internalId and Code Interco Text Value
	 * @governance 10 units
	 */
	function getVendorDetails(vendorIds){
		var columns = [];
		columns.push(searchModule.createColumn({name: 'internalid'}));
		columns.push(searchModule.createColumn({name: 'cseg_bwp_codeinterc'}));

		var filters = [];
		filters.push(searchModule.createFilter({name: 'internalid', operator: searchModule.Operator.ANYOF, values: vendorIds}));

		// Following 2 lines are used for debugging purposes by using hard coded internalIds of existing Vendors
		//filters.push(searchModule.createFilter({name: 'internalid', operator: searchModule.Operator.ANYOF, values: poIds}));
		//var filters = [["internalid","anyof","282","2079"]];

		var results = searchModule.create({type:searchModule.Type.VENDOR, columns: columns, filters: filters}); // 10 units

		var vendorDetails = {};
		var sr = results.run().each(function(vendor){
			var intercoText = vendor.getText({name: 'cseg_bwp_codeinterc'});
			var intercoVal = vendor.getValue({name: 'cseg_bwp_codeinterc'});
			var internalId = vendor.getValue({name: 'internalid'});

			vendorDetails[internalId] = {
					codeIntercoText: intercoText,
					codeInterco: intercoVal
			};

			return true;
		});

		return vendorDetails;
	}

	/** The following function searches for all Code Interco pour Retraitement segment values to compare against Vendor's
	 *  Code Interco to match by text.
	 *
	 * @author geoffrey.wagner@gurussolutions.com modified by grant.encarnacion@gurussolutions.com
	 * @param {Object}  updateObj	: JSON object of SO, Item, Vendor information
	 * @return {Object} updateObj	: JSON object of SO, Item, Vendor information, Code Interco mapping
	 * @governance 10 units
	 */
	function updateCodeIntercoConverted(updateObj) {
		var filters = [];

		var columns = [];
		columns.push(searchModule.createColumn({name: 'internalid'}));
		columns.push(searchModule.createColumn({name: 'name'}));

		// Search on Code Interco pour Retraitement
		var codeIntercoSearch = searchModule.create({type: 'customrecord_cseg_bwp_interco_rm', filters: filters, columns: columns}); // 10 units

		// Run search and generate object with values to match
		var codeInterco = {};
		codeIntercoSearch.run().each(function(code) {
			if (!codeInterco.hasOwnProperty(code.getValue({name: 'internalid'}))) {
				codeInterco[code.getValue({name: 'name'})] = code.getValue({name: 'internalid'});
			}

	        return true;
	    });

		// Update codeIntercoConverted in updateObject with the internal id of the Code Interco pour Retraitement that matches the text of the Vendor's Code Interco
		for (var key in updateObj) {
			// If Vendor's Code Interco matches the text of the Code Interco Pour Retraitement,
			if (codeInterco.hasOwnProperty(updateObj[key].codeIntercoText)) {
				updateObj[key].codeIntercoConverted = codeInterco[updateObj[key].codeIntercoText];
			}
		}

		return updateObj;
	}

	/** The following function updates all SO lines that do not have their corresponding Code Interco pour Retraitement field populated.
	 *
	 * @author grant.encarnacion@gurussolutions.com
	 * @param soRec {Standard Record} : SO Record to be updated
	 * @param updateObj {Object} : Lookup object containing all Vendor details.
	 * @governance 0 units
	 */
	function updateLineItemsCodeIntercoRM(soRec, updateObj){

		var itemLineCount = soRec.getLineCount({sublistId: 'item'});

		for (var i = 0; i < itemLineCount; i++){
			var currentLineIntercoRM = soRec.getSublistValue({sublistId: 'item', fieldId: 'cseg_bwp_interco_rm', line: i});
			var currentLinePOVendorId = soRec.getSublistValue({sublistId: 'item', fieldId: 'povendor', line: i});

			// only consider lines with empty Code Interco Retraitement
			if (!currentLineIntercoRM && currentLinePOVendorId != null && currentLinePOVendorId != ''){
				// set the line's Code Interco RM

				log.debug('Updating code Interco Ln ' + i,  updateObj[currentLinePOVendorId].codeIntercoConverted);

				soRec.setSublistValue({
					sublistId: 'item',
					fieldId: 'cseg_bwp_interco_rm',
					line: i,
					value: updateObj[currentLinePOVendorId].codeIntercoConverted
				});
			}
		}
	}

	return {
		beforeSubmit: beforeSubmit_CodeIntercoCheck
	};
});